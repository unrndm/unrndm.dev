/** @type {import('@types/eslint').Linter.Config} */
const eslintConfig = {
  extends: ["next/core-web-vitals", "prettier"],
}

module.exports = eslintConfig
