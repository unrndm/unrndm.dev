/**
 * @type {import('@types/prettier').Config}
 */
const prettierConfig = {
  semi: false,
  singleQuote: false,
  trailingComma: "all",
  vueIndentScriptAndStyle: true,
}

module.exports = prettierConfig
