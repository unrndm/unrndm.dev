# [unrndm.dev](unrndm.dev)

## Development

> **_NOTE:_** [package.json#scripts.prettier-base](./package.json) is only tested on linux, will most lickely not work on windows, needs a MR

to run development server use your favorite node package manager (npm, yarn, pnpm) to run `dev` script, i.e.

```sh
pnpm dev
```

or use vscode task with label `package.json#scripts.dev`

both will start `next dev` and open it in your prefered browser

## TODO:

- [ ]: add commitlint
- [ ]: add stylelint
- [ ]: add tailwind

## Usefull links:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.
